package com.locationdataminer.datakom3.GeoFenceHandler;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.locationdataminer.datakom3.ViewHandler.ViewHandler;
import com.locationdataminer.datakom3.locationdataminer.MainActivity;
import com.locationdataminer.datakom3.locationdataminer.R;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.google.android.gms.maps.SupportMapFragment;



import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Mathew on 27/04/2016.
 */
public class GeoHandler {
    public LocalGeofence enteredGeofence = null;
    //The Google Map object, used to perform operations on the mapview on the startpage.
    private GoogleMap mMap;
    //Helper class for getting location
    private LocationManager locationManager;
    //Same as above
    private LocationRequest locationRequest;
    //Array to hold all the local geofence objects, which contains all the geofences we've fetched from the server
    private ArrayList<LocalGeofence> localGeofences = new ArrayList<LocalGeofence>();
    //The geofences we're sending to LocationService in the Google Api
    private ArrayList<Geofence> geoFences = new ArrayList<Geofence>();
    //The class whose responsible for creating and manipulating geofences
    private GeofenceController geoController = new GeofenceController();
    //The intent we're sending when we want an update of the local geofences
    private MainActivity mainAct;


    public GeoHandler(MainActivity ma){
        mainAct = ma;
        locationRequest = new LocationRequest();

        locationRequest.setInterval(5000);
        // We want the location to be as accurate as possible.
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    public LocationManager getLocationManager(){
        return locationManager;
    }

    public GoogleMap getMap(){
        return mMap;
    }

    public LocationRequest getLocationRequest(){
        return locationRequest;
    }


    public  void setLocationManager(LocationManager lm){
        locationManager = lm;
    }

    //Updates the geofences
    public void updateGeofences(String result) {


        //Create local geofence objects which we can manipulate
        try {
            localGeofences = geoController.geoFenceArrayCreator(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Update map with the new geofences
        updateMap();

        //Create an array which we're gonna send to google api to set up the new geofences in the LocationService;
        geoFences = geoController.createGeoFences(localGeofences);


        //Send geofences to LocationService, wait for intents
        if (!geoFences.isEmpty())
        new GoogleApiHandler(mainAct, geoFences);
    }

    //Updates the map with new geofences.
    public void updateMap() {
        mMap.clear();
        ViewHandler paint = new ViewHandler();
        for (int i = 0; i < localGeofences.size(); i++) {
            LatLng coordinates = new LatLng(localGeofences.get(i).lat, localGeofences.get(i).lng);
            String ID = localGeofences.get(i).id;
            //If the geofence we're adding to the map is one we're currently in, make its color green
            if (enteredGeofence != null && ID.equals(enteredGeofence.id)) {
                paint.addCirclePainter(mMap, localGeofences, coordinates, i, 0x9014f5d1);
            }
            //Create red circles for all geofences
            else {
                paint.addCirclePainter(mMap, localGeofences, coordinates, i, 0x90e74c3c);
            }
        }
    }

    public void updateCurrentGeofence(String id) {
        if (id.equals("None"))
            enteredGeofence = null;

        for (int i = 0; i < localGeofences.size(); i++) {
            if (id.equals(localGeofences.get(i).id)) {
                enteredGeofence = localGeofences.get(i);
                updateMap();
                return;
            }
        }

        enteredGeofence = null;
    }


    //Set up the map if we've haven't already
    public void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the
        // map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = mainAct.getSupportMapFragment();//((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }


    private void setUpMap() {
        Criteria criteria = new Criteria();
        //Get last known location
        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        if (location != null) {
            //Move camera to location definied above
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(location.getLatitude(), location.getLongitude()), 13));
        }
        //Set map settings
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.setIndoorEnabled(false);
        mMap.setMyLocationEnabled(true);

        mMap.setOnCameraChangeListener(mainAct);

    }

    public String getGroupID(String id){
        for (int i = 0; i < localGeofences.size(); i++) {
            if (id.equals(localGeofences.get(i).id))
                return localGeofences.get(i).groupid;
        }
        return "";
    }

}
