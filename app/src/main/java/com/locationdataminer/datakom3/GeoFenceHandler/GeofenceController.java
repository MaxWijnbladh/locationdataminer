package com.locationdataminer.datakom3.GeoFenceHandler;


import android.util.Log;

import com.google.android.gms.location.Geofence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class performs all operations for creating new local geofences and geofences for the LocationAPI
 *
 *
 * Created by maxwi on 4/19/2016.
 */
public class GeofenceController {
    //The array we want to send to the LocationAPI
    public ArrayList<Geofence> geoFences = new ArrayList<Geofence>();
    //The array containing our local geofences
    public ArrayList<LocalGeofence> requestedGeoFences = new ArrayList<LocalGeofence>();


    //Create the necessarry arraylist which LocationAPI accepts from the local geofences
    public ArrayList<Geofence> createGeoFences(ArrayList<LocalGeofence> requestedGeoFences) {

        for(int i = 0; i < requestedGeoFences.size(); i++) {
            LocalGeofence newGeoFence = requestedGeoFences.get(i);
            geoFences.add(new Geofence.Builder()
                    .setRequestId(newGeoFence.id)
                    .setCircularRegion(newGeoFence.lat, newGeoFence.lng, newGeoFence.rad)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .build());
        }


        return geoFences;

    }
    
    //Create a list of local geofences from the string we got from the server
    public ArrayList<LocalGeofence> geoFenceArrayCreator(String result) throws JSONException {

        JSONObject ids;
        String id;
        double lat;
        double lng;
        JSONObject loc;
        float rad;
        String track_types;
        requestedGeoFences.clear();
        JSONObject res = new JSONObject(result);
        JSONArray data = res.getJSONArray("data");

        for(int i = 0; i < data.length(); i++) {
            JSONObject object = data.getJSONObject(i);
            //      ids = object.getJSONObject("_id");
            id = object.getString("id");
            loc = object.getJSONObject("loc");
            lng = Double.parseDouble(loc.getString("lng"));
            lat = Double.parseDouble(loc.getString("lat"));
            rad = Float.parseFloat(object.getString("range"));
            ArrayList <String> operations = new ArrayList<String>();
            ArrayList <String> text = new ArrayList<String>();

            try {
                String textComp = object.getString("text");
                Log.d("Paaaaarse Regex", "Preparing to parse: "+textComp);

                String[] textSplit = textComp.split("'");
                if(textSplit.length != 9 ){
                    Log.d("Paaaaaaarse regex", "found " +Integer.toString(textSplit.length));
                    text = null;
                } else {
                    Log.d("Paaaaaarse rexex", "Actually found 9");
                    text.add(textSplit[1]);
                    text.add(textSplit[3]);
                    text.add(textSplit[5]);
                    text.add(textSplit[7]);
                    Log.d("Paaaaaarse rexex", "Out: " + text.toString());
                }
                /*
                Pattern pat = Pattern.compile("'(.*?)'");
                Matcher m = pat.matcher(textComp);

                if(m.groupCount() != 4){
                    Log.d("Paaaaaaarse regex", "found " +Integer.toString(m.groupCount()));
                    text = null;
                } else {
                    Log.d("Paaaaaarse rexex", "Actually found 4");
                    text.add(m.group(1));
                    text.add(m.group(2));
                    text.add(m.group(3));
                    text.add(m.group(4));

                }*/
            }
            catch (Exception e) {
                text = null;
                Log.d("Paaaaarse regex", "Exceptions!" + e.toString());
            }


            if (rad == 0.0) {
                rad = 10;
            }

            track_types = object.getString("track_types");

            if (track_types.toLowerCase().contains("audio")) {
                operations.add("audio");
            }
            if (track_types.toLowerCase().contains("image")) {
                operations.add("image");
            }
            if (track_types.toLowerCase().contains("video")) {
                operations.add("video");
            }
            if (track_types.toLowerCase().contains("decibel")) {
                operations.add("decibel");
            }



            Log.d("Parse", "Created Geofence with id "+ id+ ", operations "+operations+" and text "+text);
            LocalGeofence temp = new LocalGeofence(id, lat, lng, rad, operations, text);
            requestedGeoFences.add(temp);


        }

        return requestedGeoFences;
    }

























}
