package com.locationdataminer.datakom3.GeoFenceHandler;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.locationdataminer.datakom3.locationdataminer.R;

import java.util.List;

/**
 * This class handles all the incoming intents from the LocationAPI (Which it sends when we enter a geofence
 *
 *
 * Created by maxwi on 4/20/2016.
 */
public class GeofenceIntentService extends IntentService {

    public final String TAG = this.getClass().getCanonicalName();


    public GeofenceIntentService() {
        super("GeofenceIntentService");
        Log.v(TAG, "onCreate");
    }

    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "onCreate");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy");
    }

    //this method is run when intentservice recivies info that a geofence has been entered
    @Override
    protected void onHandleIntent(Intent intent) {
        //Extract the event from the intent
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        Log.v(TAG, "onHandleIntent");
        //Create a new intent which we will send to the rest of the app
        Intent actionIntent = new Intent();

        if(!geofencingEvent.hasError()) {
            //transition defines if we entered or exited a geofence
            int transition = geofencingEvent.getGeofenceTransition();
            //What text to put in the notification
            String notificationTitle;

            switch(transition) {
                case Geofence.GEOFENCE_TRANSITION_ENTER:
                    notificationTitle = "Geofence Entered";
                    actionIntent.setAction("ENTER");
                    actionIntent.putExtra("Current Geofence ID", getTriggeringGeofences(intent));
                    //Inform application of event
                    sendBroadcast(actionIntent);



                    Log.v(TAG, "Geofence Entered");
                    break;
                case Geofence.GEOFENCE_TRANSITION_DWELL:
                    notificationTitle = "Geofence Dwell";
                    Log.v(TAG, "Dwelling in Geofence");
                    break;
                case Geofence.GEOFENCE_TRANSITION_EXIT:
                    notificationTitle = "Geofence Exit";
                    actionIntent.setAction("EXIT");
                    //Inform application of event
                    sendBroadcast(actionIntent);
                    Log.v(TAG, "Geofence Exited");
                    break;
                default:
                    notificationTitle = "Geofence Unknown";
            }
            //Create an notification for the event
            //sendNotification(this, getTriggeringGeofences(intent), notificationTitle);




        }else{

            Log.v("TAAAAAAAAAAAAAAAAAAAAAG", "ErrorTerror");
        }
    }

    //create a notification on the device with info about which geofence has been entered
    private void sendNotification(Context context, String notificationText, String notificationTitle) {




        PowerManager pm = (PowerManager) context
                .getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK, "");
        wakeLock.acquire();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText)
                .setDefaults(Notification.DEFAULT_ALL).setAutoCancel(false);



        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

        wakeLock.release();
    }

    //Create a string with the geofences that triggered the intent
    private String getTriggeringGeofences(Intent intent) {
        GeofencingEvent geofenceEvent = GeofencingEvent.fromIntent(intent);
        List<Geofence> geofences = geofenceEvent
                .getTriggeringGeofences();

        String[] geofenceIds = new String[geofences.size()];

        for (int i = 0; i < geofences.size(); i++) {
            geofenceIds[i] = geofences.get(i).getRequestId();
        }

        return TextUtils.join(", ", geofenceIds);


    }


}
