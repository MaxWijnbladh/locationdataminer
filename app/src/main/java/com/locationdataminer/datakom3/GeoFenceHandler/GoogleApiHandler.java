package com.locationdataminer.datakom3.GeoFenceHandler;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;


/**
 * Created by maxwi on 4/19/2016.
 */
public class GoogleApiHandler implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status>, LocationListener {

    //Tag for debugging
    private final String TAG = this.getClass().getSimpleName();

    private Context context;
    private GoogleApiClient apiClient;
    private PendingIntent pendingIntent;
    private ArrayList<Geofence> geoFences;
    private GeofencingRequest geofencingRequest;
    private LocationRequest locationRequest;
    private PendingResult<Status> pendingResult = null;


    public GoogleApiHandler(Context context, ArrayList<Geofence> geoFences) {

        //Set initial variables
        this.context = context;
        //The geofences we want to send to the LocationServices
        this.geoFences = geoFences;
        pendingIntent = null;

        //Create GoogleApiClient, set api as LocationServices, spec connectioncallbacks and onfailed method.


        apiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();


        // This is purely optional and has nothing to do with geofencing.
        // I added this as a way of debugging.
        // Define the LocationRequest.
        locationRequest = new LocationRequest();
        // We want a location update every 10 seconds.
        locationRequest.setInterval(10000);
        // We want the location to be as accurate as possible.
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        //Connect to the LocationServices API
        apiClient.connect();
    }



    @Override
    public void onResult(Status result) {


    }
    
    //If we successfully connect to the LocationAPI, create a new geofences request and create a pendingintent for them.
    @Override
    public void onConnected(Bundle connectionHint) {
        Toast.makeText(context, "Creating new geofences", Toast.LENGTH_SHORT).show();
        Log.v(TAG, geoFences.toString());
        geofencingRequest = new GeofencingRequest.Builder().addGeofences(
                geoFences).build();
        
        //Define the pending intent for when we recieve alerts that we entered a geofence
        pendingIntent = createRequestPendingIntent();

    
        //Definite the variable holding the result from the pending intent execution.
        PendingResult<Status> pendingResult = LocationServices.GeofencingApi
                    .addGeofences(apiClient, geofencingRequest,
                            pendingIntent);


        //Set the callback method for the pending result.
        pendingResult.setResultCallback(this);

        if (pendingResult == null) {
            Log.v(TAG, "Failed to get permission");
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v(TAG, "Connection suspended.");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.v(TAG, "Location Information\n"
                + "==========\n"
                + "Provider:\t" + location.getProvider() + "\n"
                + "Lat & Long:\t" + location.getLatitude() + ", "
                + location.getLongitude() + "\n"
                + "Altitude:\t" + location.getAltitude() + "\n"
                + "Bearing:\t" + location.getBearing() + "\n"
                + "Speed:\t\t" + location.getSpeed() + "\n"
                + "Accuracy:\t" + location.getAccuracy() + "\n");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v(TAG, "FAILURE in connection");
    }

    //Create the request for a pending intent
    private PendingIntent createRequestPendingIntent() {
        if (pendingIntent == null) {
            Log.v(TAG, "Creating PendingIntent");


            //Create the intent we want the locationAPI to send when we enter a geofence
            Intent intent = new Intent(context, GeofenceIntentService.class);
            //Create the pending intent, giving the LocationAPI the permissions the execute the Intent intent.
            pendingIntent = PendingIntent.getService(context, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }

        return pendingIntent;
    }

}
