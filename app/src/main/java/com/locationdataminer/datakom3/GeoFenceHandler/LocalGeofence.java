package com.locationdataminer.datakom3.GeoFenceHandler;

import java.util.ArrayList;

/**
 * Created by max on 2016-04-19.
 */
public class LocalGeofence {

    public String id;
    public String groupid;
    public double lat;
    public double lng;
    public float rad;
    public ArrayList<String> ops;
    public ArrayList<String> text;

    public LocalGeofence(String id, String groupid, double lat, double lng, float rad, ArrayList<String> ops, ArrayList<String> text) {
        this.id = id;
        this.groupid = groupid;
        this.lat = lat;
        this.lng = lng;
        this.rad = rad;
        this.ops = ops;
        this.text = text;
    }

    public LocalGeofence(String id, double lat, double lng, float rad, ArrayList<String> ops, ArrayList<String> text) {
        this.id = id;
        this.groupid = "";
        this.lat = lat;
        this.lng = lng;
        this.rad = rad;
        this.ops = ops;
        this.text = text;
    }

    public String toString(){
        return "Geofence: "+id+" Ops:"+ops+" text"+text;
    }

}