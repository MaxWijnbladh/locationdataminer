package com.locationdataminer.datakom3.Login;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.locationdataminer.datakom3.locationdataminer.R;

public class PagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    public PagerAdapter(){
        super(null);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Fragment Intro1 = new Intro1();
                return Intro1;
            case 1:
                Fragment Intro2 = new Intro2();
                return Intro2;
            case 2:
                Fragment Intro3 = new Intro3();
                return Intro3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 0;
    }
}
