package com.locationdataminer.datakom3.SensorDataPackage;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static android.support.v4.app.ActivityCompat.startActivityForResult;

/**
 * Created by maxwi on 4/24/2016.
 */
public class AudioService extends IntentService {

        MediaRecorder recorder = new MediaRecorder();
        String filePath;
        String geofenceLat;
        String geofenceLng;
        // variables
        //a surface holder
        private SurfaceHolder sHolder;
        //a variable to control the camera
        private Camera mCamera;
        //the camera parameters
        private Camera.Parameters parameters;
        static final int REQUEST_IMAGE_CAPTURE = 1;
        String mCurrentPhotoPath;


        int RECORDING_DURATION = 6000;
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public AudioService() {
        super("Recording Service");
        Log.v("TAAAAAAAAAAAAAAAAAAAAAG", "AudioServicesssMake");
    }

    public void recordAudio() {

        filePath = getFilesDir().getAbsolutePath() + "/audiominerfile.mp3";
        Log.v("OPERATION", "Starting recording!");
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        recorder.setOutputFile(filePath);
        try {
            recorder.prepare();
            recorder.start();
            Intent recordIntent = new Intent();
            recordIntent.setAction("RECORDING_AUDIO");
            sendBroadcast(recordIntent);
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    stopRecordAudio();
                }
            }, RECORDING_DURATION);
        } catch (IOException e) {
            Log.e("giftlist", "io problems while preparing");
        }

    }

    public void stopRecordAudio() {
        recorder.stop();
        Intent stoppedRecording = new Intent();
        stoppedRecording.setAction("STOPPED_RECORDING_AUDIO");
        sendBroadcast(stoppedRecording);
        Log.v("OPERATION", "STOPPED RECORDING");
        recorder.reset();
        recorder.release();
        fileReadyToSend();
    }



    private void fileReadyToSend() {
        //File filePath = null;

        //filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS + "/test.mp3");
        //Log.d("TAG", filePath.toString());

        Intent fileSendIntent = new Intent();
        fileSendIntent.setAction("FILE_SEND");
        fileSendIntent.putExtra("FILE_PATH", filePath);
        fileSendIntent.putExtra("GeofenceLat", geofenceLat);
        fileSendIntent.putExtra("GeofenceLng", geofenceLng);
        Log.v("TAAAAAAAAAAAAAAAAAAAAAG", "SKICKAR");
        sendBroadcast(fileSendIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        geofenceLat = intent.getStringExtra("GeofenceLat");
        geofenceLng = intent.getStringExtra("GeofenceLng");
        recordAudio();

    }
}
