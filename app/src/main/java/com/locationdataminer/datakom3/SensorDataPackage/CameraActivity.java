package com.locationdataminer.datakom3.SensorDataPackage;

import android.app.IntentService;
import android.content.Intent;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.locationdataminer.datakom3.locationdataminer.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CameraActivity extends Activity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    String mCurrentPhotoPath;
    public final String TAG = this.getClass().getCanonicalName();
    File image;
    String geofenceLat;
    String geofenceLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        geofenceLat = intent.getStringExtra("GeofenceLat");
        geofenceLng = intent.getStringExtra("GeofenceLng");
        Log.v(TAG, "Inside Camera onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera2);
        /*
        new AlertDialog.Builder(CameraActivity.this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Picture Request")
                .setMessage("Take a picture of Per Gunningbergs face")
                .setPositiveButton("Accept", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v(TAG, "Sending CameraActivity Intent");
                        dispatchTakePictureIntent();
                    }

                })
                .setNegativeButton("Decline", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })

                .show();
        */
        dispatchTakePictureIntent();
    }


    private void dispatchTakePictureIntent() {
        Intent imageIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        //folder stuff
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "MyImages");
        imagesFolder.mkdirs();

        image = new File(imagesFolder, "QR_" + timeStamp + ".png");
        Uri uriSavedImage = Uri.fromFile(image);

        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        startActivityForResult(imageIntent, REQUEST_IMAGE_CAPTURE);



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Intent fileSendIntent = new Intent();
            fileSendIntent.setAction("FILE_SEND");
            fileSendIntent.putExtra("FILE_PATH", image.getAbsolutePath());
            fileSendIntent.putExtra("GeofenceLat", geofenceLat);
            fileSendIntent.putExtra("GeofenceLng", geofenceLng);
            sendBroadcast(fileSendIntent);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + mCurrentPhotoPath)));
            finish();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        Log.v(TAG, mCurrentPhotoPath);
        return image;
    }
}

