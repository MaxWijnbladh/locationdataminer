package com.locationdataminer.datakom3.SensorDataPackage;

/**
 * Created by Mathew on 20/04/2016.
 */

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import static android.support.v4.app.ActivityCompat.startActivityForResult;


public class DecibelMeter extends IntentService {


    private MediaRecorder mRecorder;
    private int maxAmp = 5;
    String geofenceLat;
    String geofenceLng;


    public DecibelMeter(){
        super("DB Service");
    }


    private double record(){
        try {
            mRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mRecorder.start();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                setMaxAmp();
            }
        }, 1500);

        return waitAndGet();
    }

    private double waitAndGet(){
        Timer t = new Timer();
        try {
            synchronized(t){
                t.wait(1600);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int max =  mRecorder.getMaxAmplitude();
        stopRecord();

        return max;
    }


    private void setMaxAmp(){
        maxAmp = mRecorder.getMaxAmplitude();
    }

    private void stopRecord() {
        mRecorder.stop();
        mRecorder.reset();
        mRecorder.release();
    }

    private double getAmplitude() {
        if (mRecorder != null)
            return  (mRecorder.getMaxAmplitude());
        else
            return 0;
    }

    public void getDB(){
        //Setup
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setOutputFile("/dev/null");
        Log.v("TAAAAAAAAAAAAAAAAAAAAAG", "MEASURE DB");

        //performe
        double reference = 1;
        fileReadyToSend(20 * Math.log10(record()/ reference));
    }


    private void fileReadyToSend(double db) {
        Intent fileSendIntent = new Intent();
        fileSendIntent.setAction("FILE_SEND");
        fileSendIntent.putExtra("FILE_PATH", "" + db);
        fileSendIntent.putExtra("GeofenceLat", geofenceLat);
        fileSendIntent.putExtra("GeofenceLng", geofenceLng);
        Log.v("TAAAAAAAAAAAAAAAAAAAAAG", "sending DB");
        sendBroadcast(fileSendIntent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        geofenceLat = intent.getStringExtra("GeofenceLat");
        geofenceLng = intent.getStringExtra("GeofenceLng");
        getDB();
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
