package com.locationdataminer.datakom3.SensorDataPackage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Camera;
import android.hardware.Camera.CameraInfo;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.locationdataminer.datakom3.locationdataminer.R;

import java.io.File;

/**
 * Created by maxwi on 5/17/2016.
 */
public class VideoCapture extends Activity {

    static final int REQUEST_VIDEO_CAPTURE = 101;
    Intent takeVideoIntent;
    String filePath;
    File video;
    String geofenceLat;
    String geofenceLng;

    protected void onCreate(Bundle savedInstanceState) {
        Log.v("VideoCapture", "Inside Camera onCreate");
        Intent intent = getIntent();
        geofenceLat = intent.getStringExtra("GeofenceLat");
        geofenceLng = intent.getStringExtra("GeofenceLng");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera2);
        dispatchTakeVideoIntent();
    }


    private void dispatchTakeVideoIntent() {
        takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);


        File videoFolder = new File(Environment.getExternalStorageDirectory(), "MyVideos");
        videoFolder.mkdirs();

        video = new File(videoFolder, "QR_.mp4");
        Uri uriSavedVideo = Uri.fromFile(video);

        takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedVideo);

        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {

            Intent fileSendIntent = new Intent();
            fileSendIntent.setAction("FILE_SEND");
            fileSendIntent.putExtra("FILE_PATH", video.getAbsolutePath());
            fileSendIntent.putExtra("GeofenceLat", geofenceLat);
            fileSendIntent.putExtra("GeofenceLng", geofenceLng);
            sendBroadcast(fileSendIntent);;
            finish();


        }
    }

}



