package com.locationdataminer.datakom3.ServerCommunicator;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

/**
*This class is responsible for handling all connections to the server .
*
*
* Created by max on 2016-04-21.
 */
public class ServerConnector extends Service {
    //How often to we want to poll the server for updates
    public static final long NOTIFY_INTERVAL = 3600 * 1000;
    //Handler for the new thread we're sending GET requests with
    private Handler handler = new Handler();
    //The timer object which helps set the polling for set intervals.
    private Timer timer = null;
    //The result from the input stream
    String result;
    //The response from the server
    String response;
    //The geofences we already have in memory
    String currentGeofences;
    //The URL we want to post files to
    String postURL = "okej.cloudapp.net:5000/position";
    List<NameValuePair> pairs;

    //When the service is created do this.
    @Override
    public void onCreate() {
        //The filter which decides which broadcasts we recieve
        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("APP_START");
        intentfilter.addAction("UPDATE");
        intentfilter.addAction("FILE_SEND");
        //Init the new broadcast reciever.
        BroadcastReceiver r = new ServerConnectorReciever();
        getApplicationContext().registerReceiver(r, intentfilter);

        response = null;
        currentGeofences = null;
        //Init the timer if we haven't already
        if (timer != null) {
            timer.cancel();
        } else {
            timer = new Timer();
        }
        //Set the timer so it performs the actions defined in getDataFromServer at the NOTIFY_INTERVAL
        timer.scheduleAtFixedRate(new getDataFromServer(), 0, NOTIFY_INTERVAL);

    }
    //This class starts a new thread and performs the actions in the run function.

    class getDataFromServer extends TimerTask {

        @Override
        public void run() {
            handler.post(new Runnable() {

                @Override
                public void run() {

                    try {
                        //Get geofences from server
                        response = new HttpAsyncTask().execute("http://okej.cloudapp.net:5000/track").get();
                        Log.v("Response Length", String.valueOf(response.length()));
                        //See if the server has provided us with any new geofences.
                        if (response.equals(currentGeofences)) {
                            //If no updates create a toast indicating this
                            Log.v("TAAAAAG", "No Update from server");
                            Toast.makeText(getApplicationContext(), "No update from server", Toast.LENGTH_SHORT).show();

                        } else {
                            //Create toast indicating we've fetched an update and send the updated geofences to the MainActivity
                            sendUpdate();

                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }


                }
            });
        }
    }
    //Send updates geofences to MainActivity
    public void sendUpdate() {
        currentGeofences = response;
        Log.v("TAAAAAAG", "NEW UDPATE FROM SERVER");
        Intent updateIntent = new Intent();
        updateIntent.setAction("DATAUPDATE");
        updateIntent.putExtra("Updated Geofences", currentGeofences);
        sendBroadcast(updateIntent);
    }
    
    
    public String encodeToB64(File file) {
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String b64String = Base64.encodeToString(bytes, Base64.NO_WRAP);

        return b64String;
    }
    
    
    //Send file to postURL
    public String sendAudio(String b64String, String lng, String lat) {
     try {
            pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("data", "data:audio/mp3;base64," + b64String));
            pairs.add(new BasicNameValuePair("datatype", "audio"));
            pairs.add(new BasicNameValuePair("lng", lng));
            pairs.add(new BasicNameValuePair("lat", lat));


        } catch (Exception e) {
            Log.v("FAIL", "MEGAFAIL");
        }
        Log.v("Sending this object", pairs.toString());

        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {

                    HttpClient client = new DefaultHttpClient ();
                    HttpPost post = new HttpPost ("http://okej.cloudapp.net:5000/position");
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    post.setEntity(new UrlEncodedFormEntity(pairs, HTTP.UTF_8));
                    HttpResponse response = client.execute(post);
                    Log.v("TAG", "FILE SENT");





                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Log.v("KAN SKE", "STARTING THREAD");
        thread.start();

        return "Done";
    }

    public String sendVideo(String b64String, String lng, String lat) {
        try {
            pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("data", "data:video/mp4;base64," + b64String));
            pairs.add(new BasicNameValuePair("datatype", "video"));
            pairs.add(new BasicNameValuePair("lng", lng));
            pairs.add(new BasicNameValuePair("lat", lat));


        } catch (Exception e) {
            Log.v("FAIL", "MEGAFAIL");
        }
        Log.v("Sending this object", pairs.toString());

        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {

                    HttpClient client = new DefaultHttpClient ();
                    HttpPost post = new HttpPost ("http://okej.cloudapp.net:5000/position");
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    post.setEntity(new UrlEncodedFormEntity(pairs, HTTP.UTF_8));
                    HttpResponse response = client.execute(post);
                    Log.v("TAG", "FILE SENT");





                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Log.v("KAN SKE", "STARTING THREAD");
        thread.start();

        return "Done";
    }

    public String sendPhoto(String b64String, String lng, String lat, String type) {
        try {
            pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("data", "data:image/"+type+";base64," + b64String));
            pairs.add(new BasicNameValuePair("datatype", "image"));
            pairs.add(new BasicNameValuePair("lng", lng));
            pairs.add(new BasicNameValuePair("lat", lat));


        } catch (Exception e) {
            Log.v("FAIL", "MEGAFAIL");
        }
        Log.v("Sending this object", pairs.toString());

        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {

                    HttpClient client = new DefaultHttpClient ();
                    HttpPost post = new HttpPost ("http://okej.cloudapp.net:5000/position");
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    post.setEntity(new UrlEncodedFormEntity(pairs, HTTP.UTF_8));
                    HttpResponse response = client.execute(post);
                    Log.v("TAG", "FILE SENT");





                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Log.v("KAN SKE", "STARTING THREAD");
        thread.start();

        return "Done";
    }

    public String sendDecibel(double deciB, String lng, String lat) {
        try {
            pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("data", "" + deciB));
            pairs.add(new BasicNameValuePair("datatype", "decibel"));
            pairs.add(new BasicNameValuePair("lng", lng));
            pairs.add(new BasicNameValuePair("lat", lat));


        } catch (Exception e) {
            Log.v("FAIL", "MEGAFAIL");
        }
        Log.v("Sending this object", pairs.toString());

        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {

                    HttpClient client = new DefaultHttpClient ();
                    HttpPost post = new HttpPost ("http://okej.cloudapp.net:5000/position");
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    post.setEntity(new UrlEncodedFormEntity(pairs, HTTP.UTF_8));
                    HttpResponse response = client.execute(post);
                    Log.v("TAG", "FILE SENT");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Log.v("KAN SKE", "STARTING THREAD");
        thread.start();

        return "Done";
    }

    //Create a connection to the server and fetch data from the inputstream
    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }
    //Converts to inputstream to a string we can play with
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
    //A  thread which creates a GET request to server
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            //Notify when called server?
            Log.v("TAAAAAAAAAG", "Notified serveer");
        }
    }


    private boolean isDecibel(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    //This class handles all incoming intents 
    public class ServerConnectorReciever extends BroadcastReceiver {
        //The action definied in the intent
        String ACTION;
        //The extra data appended to the intent
        Bundle bundle;
        //When intent recieved do this
        @Override
        public void onReceive(Context context, Intent intent) {

            ACTION = intent.getAction();
            bundle = intent.getExtras();
            File file = null;
            //The app is started, send the most recent geofences
            if (ACTION.equals("APP_START")) {
                sendUpdate();
                }
            //The UPDATE GEOFENCES button is clicked, request update from server
            if (ACTION.equals("UPDATE")) {
                Log.v("SHOULD", "WORK?");
                new getDataFromServer().run();
                }
            //We have a file to send to the server, send it
            else if (ACTION.equals("FILE_SEND")) {
                if (isDecibel(bundle.getString("FILE_PATH")) ) {
                    Criteria criteria = new Criteria();
                    LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));

                    double db =  Double.parseDouble(bundle.getString("FILE_PATH"));
                    Log.v("SENDING", bundle.getString("FILE_PATH"));
                    try {
                        sendDecibel(db, Double.toString(location.getLongitude()), Double.toString(location.getLatitude()));
                    }
                    catch (NullPointerException ex){
                        sendDecibel(db, bundle.getString("GeofenceLat"), bundle.getString("GeofenceLng"));
                    }
                } else{
                    Criteria criteria = new Criteria();
                    LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
                    String path = bundle.getString("FILE_PATH");
                    file = new File(path);
                    Log.v("SENDING", path);
                    if(path.endsWith(".jpg")){
                        sendPhoto(encodeToB64(file), Double.toString(location.getLongitude()), Double.toString(location.getLatitude()), "jpg");
                    } else if(path.endsWith(".png")){
                        sendPhoto(encodeToB64(file), Double.toString(location.getLongitude()), Double.toString(location.getLatitude()), "png");
                    } else if(path.endsWith(".mp3")){
                        sendAudio(encodeToB64(file), Double.toString(location.getLongitude()), Double.toString(location.getLatitude()));
                    } else if(path.endsWith(".mp4")){
                        sendVideo(encodeToB64(file), Double.toString(location.getLongitude()), Double.toString(location.getLatitude()));
                    } else {
                        Log.v("SEEEEEENDDDDDDDIIING", "FAAAAAAILED: Unknown Filetype");
                    }

                }
                }

            }

        }
    }






