package com.locationdataminer.datakom3.ViewHandler;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.locationdataminer.datakom3.GeoFenceHandler.LocalGeofence;

import java.util.ArrayList;

/**
 * Created by Mathew on 27/04/2016.
 */
public class ViewHandler {


    public ViewHandler(){

    }



    public void addCirclePainter(GoogleMap mMap, ArrayList<LocalGeofence> localGeofences, LatLng coordinates, int geoNum, int colour){
        mMap.addCircle(new CircleOptions().center(coordinates)
                .radius(localGeofences.get(geoNum).rad)
                .fillColor(colour)
                .strokeColor(colour).strokeWidth(3));

    }


}
