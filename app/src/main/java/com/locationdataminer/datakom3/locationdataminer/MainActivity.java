package com.locationdataminer.datakom3.locationdataminer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.locationdataminer.datakom3.GeoFenceHandler.GeoHandler;
import com.locationdataminer.datakom3.GeoFenceHandler.LocalGeofence;
import com.locationdataminer.datakom3.SensorDataPackage.AudioService;
import com.locationdataminer.datakom3.SensorDataPackage.CameraActivity;
import com.locationdataminer.datakom3.SensorDataPackage.DecibelMeter;
import com.locationdataminer.datakom3.SensorDataPackage.VideoCapture;
import com.locationdataminer.datakom3.ServerCommunicator.ServerConnector;

import java.util.ArrayList;
import java.util.Random;


public class MainActivity extends FragmentActivity implements GoogleMap.OnCameraChangeListener, LocationListener, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    Intent updateIntent = new Intent();

    private GeoHandler geoHandler = new GeoHandler(this);
    private String lastSeenGroupID = "";
    Animation animation = null;
    ImageView microphone;
    public final String TAG = this.getClass().getCanonicalName();
    GoogleSignInAccount acct = null;
    GoogleApiClient mGoogleApiClient;
    private int REQUEST_TUTORIAL = 150;
    SharedPreferences sharedpreferences;
    String account = "Max Wijnbladh";



    //When app starts do this
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //Select the view we want to display when the activity is started

        setContentView(R.layout.activity_main);
        sharedpreferences = getApplicationContext().getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);

        //String account = sharedpreferences.getString("Email", "");

        if (account != null) {
            Toast.makeText(getApplicationContext(), "Logged in as " + account,
                    Toast.LENGTH_LONG).show();
        }
        //Initilize the button which updates the local geofences.
        FloatingActionButton button = (FloatingActionButton) findViewById(R.id.updateButton);
        //Set so this acitivty listens for button clicks.
        button.setOnClickListener(this);

        animation = new AlphaAnimation(1, 0); // Change alpha from fully visible to invisible
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
        animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
        microphone = (ImageView) findViewById(R.id.microphone);
        microphone.setVisibility(View.INVISIBLE);


        //Create the filter which decides which intents to respond to in the reciever.
        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("ENTER");
        intentfilter.addAction("EXIT");
        intentfilter.addAction("DATAUPDATE");
        intentfilter.addAction("RECORDING_AUDIO");
        intentfilter.addAction("STOPPED_RECORDING_AUDIO");


        //Create and register the reciever which recieves the intents from other parts of the app
        BroadcastReceiver r = new EventReciever();
        getApplicationContext().registerReceiver(r, intentfilter);

        //Create and send an intent to the ServerConnector that app has started, so we get an update
        updateIntent.setAction("APP_START");
        sendBroadcast(updateIntent);

        //Location variables, so we can get info on current location

        geoHandler.setLocationManager((LocationManager) getSystemService(Context.LOCATION_SERVICE));

        //Set up the map which display location and active geofences
        geoHandler.setUpMapIfNeeded();
        //Start the ServerConnector which polls the server for data updates
        startService(new Intent(this, ServerConnector.class));
    }



    public GoogleMap getSupportMapFragment(){
        return ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
    }

    //Action to perform when the button is clicked.
    @Override
    public void onClick(View v) {
        updateIntent.setAction("UPDATE");
        sendBroadcast(updateIntent);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //The EventReciever recieves messages from other parts of the app, and defines what action to take to each message
    private class EventReciever extends BroadcastReceiver {

        //The action contained in the intent
        private String ACTION;
        //The extra info appended to the action
        Bundle bundle;

        @Override
        public void onReceive(Context context, Intent intent) {
            ACTION = intent.getAction();
            bundle = intent.getExtras();

                //Intent i = new Intent(context, CameraActivity.class);
                ArrayList<String> operation;
                ArrayList<String> description = null;


                //If we recieve an intent saying we've entered a geofence do this
            if (ACTION.equals("ENTER")) {
                    String id = bundle.getString("Current Geofence ID");
                    geoHandler.updateCurrentGeofence(id);
                    String groupid = geoHandler.getGroupID(id);
                //i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //context.startActivity(i);
                    if(geoHandler.enteredGeofence==null){
                        Log.d("EVENT RECEIVER", "entered geofence was null!");
                        return;
                    }
                Log.d("EVENT RECEIVER", "Local Geofence at this point is"+geoHandler.enteredGeofence);
                    operation = geoHandler.enteredGeofence.ops;
                Log.d("EVENT RECEIVER", "Operations at this point are: "+operation);
                    description = geoHandler.enteredGeofence.text;
                if(description != null) {
                    Log.d("EVENT RECEIVER", "Descriptions at this point are:"+description.toString());
                } else {
                    Log.d("EVENT RECEIVER", "Descriptions at this point are: null");
                }

                //if(groupid.equals("") || !lastSeenGroupID.equals(groupid)){ //If no groupid is defined, then let it pass. If it doesn't match the last seen, then let it pass.
                    lastSeenGroupID = groupid;
                    createNotifications(context, operation, description, geoHandler.enteredGeofence);
                //}
            }

                //If we recieve an intent saying we've exited a geofence do this
            else if (ACTION.equals("EXIT")) {
                geoHandler.updateCurrentGeofence("None");
            }
            //If we recieve an intent saying we've got a pending update to the local geofences to this
            else if (ACTION.equals("DATAUPDATE")) {
                String fetchedGeofences = bundle.getString("Updated Geofences");
                geoHandler.updateGeofences(fetchedGeofences);
            }
            else if (ACTION.equals("RECORDING_AUDIO")) {
                Toast.makeText(getApplicationContext(), "Recording Audio", Toast.LENGTH_SHORT).show();
                microphone.startAnimation(animation);
                microphone.setVisibility(View.VISIBLE);
            }
            else if (ACTION.equals("STOPPED_RECORDING_AUDIO")) {
                Toast.makeText(getApplicationContext(), "Audio file sent", Toast.LENGTH_SHORT).show();
                microphone.clearAnimation();
                microphone.setVisibility(View.INVISIBLE);
            }
        }


    }

    private void createNotifications(Context context, ArrayList<String> operations, ArrayList<String> description, LocalGeofence geofence) {

        Intent i;
        String notificationTitle = "";
        int icon = -1;
        PendingIntent pendingIntent;

        i = new Intent(context, SensorActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("Description", description);
        if(description != null) {
            Log.d("CREATE NOTIFICATION", "Descriptions at this point are:"+description.toString());
        } else {
            Log.d("CREATE NOTIFICATION", "Descriptions at this point are: null");
        }
        i.putExtra("Operations", operations);
        i.putExtra("GeofenceID", geofence.id);
        i.putExtra("GeofenceLat", Double.toString(geofence.lat));
        i.putExtra("GeofenceLng", Double.toString(geofence.lng));
        pendingIntent = PendingIntent.getActivity(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
        icon = R.mipmap.ic_launcher;
        notificationTitle = "Location Data Request";
        sendNotifications(context, "See requested data in the app", pendingIntent, notificationTitle, icon);

        /*
        if (operations.contains("image")) {
            i = new Intent(context, CameraActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            pendingIntent = PendingIntent.getActivity(context, 0, i,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            icon = R.drawable.ic_camera;
            notificationTitle = "Picture Request";
            sendNotifications(context, description, pendingIntent, notificationTitle, icon);
        }
        if (operations.contains("audio")) {
            i = new Intent(context, AudioService.class);
            pendingIntent = PendingIntent.getService(context, 0, i,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            notificationTitle = "Audio Request";
            //icon = R.drawable.microphone;
            sendNotifications(context, description, pendingIntent, notificationTitle, icon);
        }
        if (operations.contains("video")) {

            i = new Intent(context, VideoCapture.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            pendingIntent = PendingIntent.getActivity(context, 0, i,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            icon = R.drawable.ic_camera;
            sendNotifications(context, description, pendingIntent, notificationTitle, icon);
        }
        if (operations.contains("decibel")) {
            i = new Intent(context, DecibelMeter.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            pendingIntent = PendingIntent.getService(context, 0, i,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            //icon = R.drawable.ic_camera;
            notificationTitle = "Decibel Request";

            sendNotifications(context, description, pendingIntent, notificationTitle, icon);
        }
        else
            i = null;
            */
    }

    private void sendNotifications(Context context, String description, PendingIntent pendingIntent, String notificationTitle , int icon) {

        PowerManager pm = (PowerManager) context
                .getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK, "");
        wakeLock.acquire();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
                context).setSmallIcon(icon)
                .setContentTitle(notificationTitle)
                .setContentText(description)
                .setColor(Color.parseColor("#3589ff"))
                .addAction(R.drawable.ic_confirm, "Accept", pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL).setAutoCancel(false);
        Random rand = new Random();
        int randomNum = rand.nextInt((100 - 0) + 1) + 0;
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(randomNum, notificationBuilder.build());
        wakeLock.release();
    }


    @Override
    public void onCameraChange(CameraPosition position) {
    }

    @Override
    public void onLocationChanged(Location location) {
        geoHandler.getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(location.getLatitude(), location.getLongitude()), 13));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == REQUEST_TUTORIAL) {
            //GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.v(TAG, "SUCCESS");

        }

    }

}



