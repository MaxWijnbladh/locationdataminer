package com.locationdataminer.datakom3.locationdataminer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.locationdataminer.datakom3.SensorDataPackage.AudioService;
import com.locationdataminer.datakom3.SensorDataPackage.CameraActivity;
import com.locationdataminer.datakom3.SensorDataPackage.DecibelMeter;
import com.locationdataminer.datakom3.SensorDataPackage.VideoCapture;

import java.util.ArrayList;

public class SensorActivity extends Activity {

    String geofenceLat;
    String geofenceLng;
    String tag = "Sensor Activity: ";
    private Button buttonVideo;
    private Button buttonAudio;
    private Button buttonPhoto;
    private Button buttonDB;
    private TextView textGeofenceID;
    private TextView textVideoDescription;
    private TextView textAudioDescription;
    private TextView textPhotoDescription;
    private TextView textDBDescription;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);
        Log.d(tag, "onCreate!");

        buttonVideo = (Button) findViewById(R.id.buttonVideo);
        buttonAudio = (Button) findViewById(R.id.buttonAudio);
        buttonPhoto = (Button) findViewById(R.id.buttonPhoto);
        buttonDB = (Button) findViewById(R.id.buttonDB);
        textGeofenceID = (TextView) findViewById(R.id.geofenceID);
        textVideoDescription = (TextView) findViewById(R.id.textMessageVideo);
        textAudioDescription = (TextView) findViewById(R.id.textMessageAudio);
        textPhotoDescription = (TextView) findViewById(R.id.textMessagePhoto);
        textDBDescription = (TextView) findViewById(R.id.textMessageDB);

        Intent i = getIntent();
        geofenceLat = i.getStringExtra("GeofenceLat");
        geofenceLng = i.getStringExtra("GeofenceLng");
        ArrayList<String> desc = i.getStringArrayListExtra("Description");
        ArrayList<String> ops = i.getStringArrayListExtra("Operations");
        String id = i.getStringExtra("GeofenceID");

        setEnabledButtons(ops, id);
        setDescriptions(desc);

    }

    protected void onStart(){
        super.onStart();
        Log.d(tag, "onStart!");
    }

    private void setEnabledButtons(){
        buttonVideo.setEnabled(true);
        buttonAudio.setEnabled(true);
        buttonPhoto.setEnabled(true);
        buttonDB.setEnabled(true);
        textGeofenceID.setText(R.string.suprdead);
    }

    private void setDescriptions(ArrayList<String> desc){
        if(desc == null){
            Log.d("SENSORACTIVITY", "Descritpions were null!");
            return;
        }

        if(desc.size() != 4){
            Log.d("SENSORACTIVITY", "Invalid number of descriptions! "+Integer.toString(desc.size()));
            return;
        }

        textAudioDescription.setText(desc.get(0));
        textAudioDescription.setVisibility(View.VISIBLE);
        textVideoDescription.setText(desc.get(1));
        textVideoDescription.setVisibility(View.VISIBLE);
        textPhotoDescription.setText(desc.get(2));
        textPhotoDescription.setVisibility(View.VISIBLE);
        textDBDescription.setText(desc.get(3));
        textDBDescription.setVisibility(View.VISIBLE);
    }

    private void setEnabledButtons(ArrayList<String> operations, String id){
        if(operations == null || operations.size() == 0){
            setEnabledButtons();
            return;
        }

        Log.d("LISTING OPERATIONS", ":"+operations);

        if(operations.contains("video"))
            buttonVideo.setEnabled(true);
        if(operations.contains("audio"))
            buttonAudio.setEnabled(true);
        if(operations.contains("image"))
            buttonPhoto.setEnabled(true);
        if(operations.contains("decibel"))
            buttonDB.setEnabled(true);
        textGeofenceID.setText(id);
    }

    public void onClickVideo(View v){
        Log.d(tag, "Click Video");
        Intent i = new Intent(this, VideoCapture.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("GeofenceLat", geofenceLat);
        i.putExtra("GeofenceLng", geofenceLng);
        startActivity(i);

    }

    public void onClickAudio(View v){
        Log.d(tag, "Click Audio");
        Intent i = new Intent(this, AudioService.class);
        i.putExtra("GeofenceLat", geofenceLat);
        i.putExtra("GeofenceLng", geofenceLng);
        startService(i);
    }

    public void onClickPhoto(View v){
        Log.d(tag, "Click Photo");
        Intent i = new Intent(this, CameraActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("GeofenceLat", geofenceLat);
        i.putExtra("GeofenceLng", geofenceLng);
        startActivity(i);
    }

    public void onClickDB(View v){
        Log.d(tag, "Click DB");
        Intent i = new Intent(this, DecibelMeter.class);
        i.putExtra("GeofenceLat", geofenceLat);
        i.putExtra("GeofenceLng", geofenceLng);
        startService(i);
    }
}
